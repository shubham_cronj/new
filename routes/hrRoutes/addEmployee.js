var express = require("express");
var router = express.Router();
employeeArray = new Array();
var employeeCount = 0;

router.post('/', function (request , response) {
 //   response.end("in hr create");

    const employeeObject = request.body;
    const firstName = employeeObject.firstName;
    const lastName = employeeObject.lastName;
    const age = employeeObject.age;
    const username = employeeObject.username;
    const password = employeeObject.password;
    var isEmployeeExist = false;

    if(!firstName || !lastName || !age || !username || !password)
    {
        response.end("All fields need to be filled");
    }
    else{
        for(i=0;i<employeeCount;i++)
        {
            if(username == employeeArray[i].username)
            {
                isEmployeeExist = true;
            }
        }
        if(!isEmployeeExist)
        {
            employeeArray.push(employeeObject);
            employeeCount++;
            response.write("employee no."+employeeCount+" registered\n");
            response.write("Registered users:\n");
            for(i=0;i<employeeCount;i++)
            {
                response.write(employeeArray[i].firstName + "\n");
            }
            // response.write(exports.employeeArray.toString("ascii"));
            response.write(JSON.stringify(employeeArray));
            response.end();
        }
        else
            response.end("employee with same username exists");
    }

});

module.exports = router;