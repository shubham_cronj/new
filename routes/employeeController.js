var express = require("express");
var router = express.Router();
var myProfile = require('./employeeRoutes/myProfile');
var myRemarks = require('./employeeRoutes/myRemarks');

router.use('/myProfile', myProfile);
router.use('/myRemarks', myRemarks);


router.post('/', function (request , response) {
    response.end("in employee");
});

module.exports = router;